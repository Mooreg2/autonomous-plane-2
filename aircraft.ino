#include <TinyGPS++.h>
#include <Servo.h>
#include <Adafruit_LSM303_U.h>

const int SERVO_MIN = 1000;
const int SERVO_MAX = 2000;

// Accelerometer
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);

// The TinyGPS++ object
TinyGPSPlus gps;

int elevatorPin = 9;
int rudderPin = 10;
int escPin = 11;
int ledPin = 12;

Servo elevator, rudder, esc;

double latitude;
double longitude;
double altitude;
double pitch;
double initialLatitude;
double initialLongitude;
double initialAltitude;
double initialPitch;

void setup() {
  
  // Initialize GPS serial port
  Serial1.begin(9600);
  
  // Attach servo pins and set initial servo positions
  elevator.attach(elevatorPin);
  elevator.write(45);
  rudder.attach(rudderPin);
  rudder.write(90);
  esc.attach(escPin);

  // Attach LED pin
  pinMode(ledPin, OUTPUT);

  // Wait for a GPS fix
  while (!updateGps()) {
    flash(3,200);
  }
  initialLatitude = gps.location.lat();
  initialLongitude = gps.location.lng();
  initialAltitude = gps.altitude.meters();

  // Arm electronic speed control
  flash(5,400);
  armEsc();

  // Wait for accelerometer to initialize
  while (!accel.begin()) {
    flash(1,100);
  }

  // Get initial pitch (if out of bounds, abort flight)
  flash(10,250);
  updatePitch();
  initialPitch = pitch;
  if (initialPitch < -5.0 || initialPitch > 5.0) {
    // initial pitch is out of bounds... abort flight
    while(1) {
      flash(1,100);
    }
  }
}

/*
 * Returns true if GPS has a valid update;
 * does not block.
 */
boolean updateGps() {
    while (Serial1.available() > 0) {
      gps.encode(Serial1.read());
    }
    if (gps.location.isValid() && gps.location.isUpdated()) {
      latitude = gps.location.lat();
      longitude = gps.location.lng();
      altitude = gps.altitude.meters();
    }
    return gps.location.isValid();
}

void updatePitch() {
  sensors_event_t event;
  accel.getEvent(&event);
  pitch = event.acceleration.x * 9.0; // scale from 0.0-10.0 to 0.0-90.0
}

/*
 * flash LED... do not use in flight! (calls delay())
 */
void flash(int count, int frequency) {
  for (int i = 0; i < count; i++) {
    digitalWrite(ledPin, HIGH);
    delay(frequency);
    digitalWrite(ledPin, LOW);
    delay(frequency);
  }
}

void armEsc() {
  // Mimics stick movements from radio control...
  // go to min, wait, then max, wait, then min.
  esc.writeMicroseconds(SERVO_MIN);
  delay(2000);
  esc.writeMicroseconds(SERVO_MAX);
  delay(2000);
  esc.writeMicroseconds(SERVO_MIN);
  delay(2000);
}

void loop() {
  // put your main code here, to run repeatedly:

}